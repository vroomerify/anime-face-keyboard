package com.vedantroy.animefacekeyboard.home

import android.content.Intent
import android.os.Bundle
import android.provider.Settings.ACTION_INPUT_METHOD_SETTINGS
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.vedantroy.animefacekeyboard.R
import kotlinx.android.synthetic.main.fragment_feedback.view.*


class FeedbackFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val mainview = inflater.inflate(R.layout.fragment_feedback, container, false )

        mainview.reportBugButton.setOnClickListener {
            Log.d("VED-APP", "Reporting Bug...")
            startActivity(Intent(ACTION_INPUT_METHOD_SETTINGS))
        }

        return mainview
    }

    companion object {
        fun newInstance() = FeedbackFragment()
    }
}
