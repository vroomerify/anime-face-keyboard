package com.vedantroy.animefacekeyboard.home.tutorial


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.vedantroy.animefacekeyboard.R


/**
 * A simple [Fragment] subclass.
 *
 */
class UseKeyboardFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_use_keyboard, container, false)
    }

    companion object {
        fun newInstance() = UseKeyboardFragment()
    }

}
