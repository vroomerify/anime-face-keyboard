package com.vedantroy.animefacekeyboard.home.tutorial

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.view.ViewCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.vedantroy.animefacekeyboard.R
import kotlinx.android.synthetic.main.fragment_tutorial.view.*



class TutorialFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val tutorialView = inflater.inflate(R.layout.fragment_tutorial, container, false )
        return tutorialView.apply {
            tutorialViewPager.adapter = TutorialFragmentPagerAdapter(childFragmentManager)
            circleIndicator.setViewPager(tutorialViewPager)
        }
    }

    companion object {
        fun newInstance() = TutorialFragment()
    }
}
