package com.vedantroy.animefacekeyboard.home.tutorial

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log

class TutorialFragmentPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {



    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> EnableKeyboardFragment.newInstance()
            1 -> UseKeyboardFragment.newInstance()
            else -> null
        }
    }

    override fun getPageTitle(position: Int): CharSequence = ""

    override fun getCount(): Int {
        return 2
    }



}