package com.vedantroy.animefacekeyboard.keyboard

import android.content.ClipDescription
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.v13.view.inputmethod.InputConnectionCompat
import android.support.v13.view.inputmethod.InputContentInfoCompat
import android.support.v4.content.FileProvider
import android.view.inputmethod.EditorInfo
import com.bumptech.glide.load.resource.gif.GifDrawable
import java.io.File
import java.io.FileOutputStream
import java.nio.ByteBuffer
import android.support.v13.view.inputmethod.EditorInfoCompat



class ContentCommitter(private val context: AnimeFaceKeyboard, val localDirectory : File) {

    private val AUTHORITY = "com.vedantroy.animefacekeyboard.commitcontent.ime.inputcontent"

    init {
        localDirectory.mkdirs()
    }

    fun supportedTypes(editorInfo: EditorInfo): Array<out String> {
        return EditorInfoCompat.getContentMimeTypes(editorInfo)
    }

    private fun commitGeneric(file : File, fileType : String) {


        val contentURI = FileProvider.getUriForFile(context, AUTHORITY, file)

        val editorInfo = context.currentInputEditorInfo
        val flag : Int

        if(Build.VERSION.SDK_INT >= 25) {
            flag = InputConnectionCompat.INPUT_CONTENT_GRANT_READ_URI_PERMISSION
        } else {
            flag = 0

            context.grantUriPermission(
                    editorInfo.packageName,
                    contentURI,
                    Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }

        val inputConnection = context.currentInputConnection

        //No Description is Temporary
        val inputContentInfo = InputContentInfoCompat(contentURI, ClipDescription("No Description", arrayOf("image/$fileType")), null)

        InputConnectionCompat.commitContent(inputConnection, editorInfo, inputContentInfo, flag, null)

    }

    fun commitGifDrawable(gifDrawable: Drawable, fileName : String) {
        val gifFile = File(localDirectory, fileName)

        if(!gifFile.exists()) {
            val clonedGifDrawable = (gifDrawable.constantState.newDrawable().mutate()) as GifDrawable
            gifDrawableToFile(clonedGifDrawable, gifFile)
        }

        commitGeneric(gifFile, "gif")
    }

    private fun gifDrawableToFile(gifDrawable: GifDrawable, gifFile : File) {

        val byteBuffer = gifDrawable.buffer
        val output = FileOutputStream(gifFile)
        val bytes = ByteArray(byteBuffer.capacity())
        (byteBuffer.duplicate().clear() as ByteBuffer).get(bytes)
        output.write(bytes, 0 ,bytes.size)
        output.close()
    }

     fun urlToName(string: String): String {
        return string.replace(Regex("[/:\\\\]"),"")
    }


}